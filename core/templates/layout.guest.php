<!DOCTYPE html>
<html class="ng-csp" data-placeholder-focus="false" lang="<?php p($_['language']); ?>" >
<head data-requesttoken="<?php p($_['requesttoken']); ?>">
    <meta charset="utf-8">
    <title>
        <?php p($theme->getTitle()); ?>
    </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--    <meta http-equiv="Content-Security-Policy" content="default-src *; style-src 'self' 'unsafe-inline'; script-src * 'unsafe-inline' 'unsafe-eval'">-->
<!--    <meta http-equiv="Content-Security-Policy" content="connect-src wss://mqtt.acheckin.io:8883 wss">-->
    <meta name="referrer" content="never">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-itunes-app" content="app-id=<?php p($theme->getiTunesAppId()); ?>">
    <meta name="theme-color" content="<?php p($theme->getColorPrimary()); ?>">
    <meta name="theme-color" content="<?php p($theme->getColorPrimary()); ?>">
    <link rel="icon" href="<?php print_unescaped(image_path('', 'favicon.ico')); /* IE11+ supports png */ ?>">
    <link rel="apple-touch-icon-precomposed" href="<?php print_unescaped(image_path('', 'favicon-touch.png')); ?>">
    <link rel="mask-icon" sizes="any" href="<?php print_unescaped(image_path('', 'favicon-mask.svg')); ?>" color="<?php p($theme->getColorPrimary()); ?>">
    <link rel="manifest" href="<?php print_unescaped(image_path('', 'manifest.json')); ?>">
<!--    <link rel="stylesheet" href="/core/css/customLogin.css">-->
    <?php emit_css_loading_tags($_); ?>
    <?php emit_script_loading_tags($_); ?>
    <?php print_unescaped($_['headers']); ?>
    <script nonce="<?php p(\OC::$server->getContentSecurityPolicyNonceManager()->getNonce()) ?>" src="/core/js/qrcode.min.js"></script>
    <script nonce="<?php p(\OC::$server->getContentSecurityPolicyNonceManager()->getNonce()) ?>" src="/core/js/node_force.min.js"></script>
    <script nonce="<?php p(\OC::$server->getContentSecurityPolicyNonceManager()->getNonce()) ?>" src="/core/js/mqtt.min.js"></script>
    <script nonce="<?php p(\OC::$server->getContentSecurityPolicyNonceManager()->getNonce()) ?>" src="/core/js/jwt-decode.js"></script>

</head>
<body id="<?php p($_['bodyid']);?>">
<?php include('layout.noscript.warning.php'); ?>
<div class="wrapper">
    <div class="v-align">
        <?php if ($_['bodyid'] === 'body-login' ): ?>
            <header role="banner">
                <div id="header">
                    <div class="logo">
                        <h1 class="hidden-visually">
                            <?php p($theme->getName()); ?>
                        </h1>
                        <?php if(\OC::$server->getConfig()->getSystemValue('installed', false)
                            && \OC::$server->getConfig()->getAppValue('theming', 'logoMime', false)): ?>
                            <img src="<?php p($theme->getLogo()); ?>"/>
                        <?php endif; ?>
                    </div>
                    <h1 style="font-size:21px;color:white">Đăng Nhập Bằng Acheckin</h1>
                    <br/>
                        <div id="ring">

                        </div>
                    <div id="qrcode" style="margin-left:22px"></div>
                </div>
            </header>
        <?php endif; ?>
        <?php print_unescaped($_['content']); ?>
    </div>
</div>
<footer role="contentinfo">
    <p class="info">
        <?php print_unescaped($theme->getLongFooter()); ?>
    </p>
</footer>
<script nonce="<?php p(\OC::$server->getContentSecurityPolicyNonceManager()->getNonce()) ?>" src="/core/js/jquery.min.js"></script>
<script nonce="<?php p(\OC::$server->getContentSecurityPolicyNonceManager()->getNonce()) ?>" src="/core/js/customeQrcode.js"></script>
</body>
</html>
